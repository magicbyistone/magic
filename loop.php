

<div id="posts">
	<?php if(have_posts()): ?>
		<section class="post-list container">
		<?php while(have_posts()): the_post(); ?>
			<?php get_template_part('post', 'item'); ?>
		<?php endwhile;?>
		</section>
	<?php endif; ?>
</div>
