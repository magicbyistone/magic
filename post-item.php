<?php /* The post template */ ?>


<article class="post-item">

	<?php if(get_the_post_thumbnail()): ?>

		<figure class="post-figure">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail(); ?>
			</a>
		</figure>
		
		<div class="post-content">
			<h2 class="post-item-title h1"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php the_excerpt(); ?>
		</div>

	<?php endif; if(!get_the_post_thumbnail()):?>

		<div class="post-content-wide">
			<span class="post-date"><?php echo get_the_date(); ?></span>
			<h1 class="post-item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
			<?php the_excerpt(); ?>
		</div>
	<?php endif; ?>

</article>