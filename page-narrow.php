<?php /* Template name: Page narrow */ ?>

<?php get_header(); ?>

	<article class="container page-narrow">
		<h1 class="page-title"><?php the_title(); ?></h1>
		<div class="page-narrow-content">
			<?php if(have_posts()): ?>
				<?php the_post(); ?>
				<?php the_content(); ?>
			<?php endif; ?>
		</div>
	</article>


<?php get_footer(); ?>
