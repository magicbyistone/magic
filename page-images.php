<?php /* Template name: Images  */ ?>
<?php get_header(); ?>

<?php $images = get_field('random_images');
if( $images ): ?>
	<ul id="images-list" class="clearfix"></ul>
	<script type="text/javascript">
        var imagesArr = [ <?php foreach( $images as $image ): ?><?php echo '"' . $image['sizes']['large'] . '", ' ;  ?><?php endforeach; ?>
    	];
	</script>
<?php endif; ?>

<?php get_footer(); ?>