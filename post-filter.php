

<nav class="post-filter-container">
	<a class="post-filter-first" href="<?php site_url(); ?>/ideas/">Blog ideas 
	<img src=" <?php bloginfo('template_url'); ?>/static/img/png-icons/task-confirm.png"></a>	
	
	<div class="post-filters-container">
		<a href="#filter-them-posts" class="post-filter-last button-filter" data-toggle="post-filters">Filter Ideas</a>

		<div class="post-filters">
			<h3>Choose filter</h3>
			<a href="#index" class="button-filter-close" data-toggle="post-filters"></a>
			
			<?php 
				$args = "hide_empty=true";
				$tags = get_tags( $args );  ?>
				
				<ul class="post-filter-list">
					<?php foreach( $tags as $tag) { ?>
					<li class="post-filter-item">
						<a href="<?php site_url(); echo '/ideas/tag/' . $tag->name; ?>">
							<?php echo $tag->name; ?>
							<span class="post-filter-count"><?php echo $tag->count; ?></span>
						</a>
					</li>
					<?php } ?>
				</ul>
				
		</div>
	</div>

</nav>




