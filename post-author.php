<div class="post-author">

	<?php $id = get_the_author_meta( ID ); ?> 

	<div class="author-image">
		<?php echo get_avatar( $id ); ?>
	</div>

	<span class="author-name">
		<?php echo get_the_author_meta("user_firstname", $id) ?>
		<?php echo get_the_author_meta("user_lastname", $id) ?>
	</span>

	<div class="post-date">
		<?php  echo get_the_date(); ?>
	</div>
</div>