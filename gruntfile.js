'use strict';

var PATH_SRC = "src/",
    PATH_BUILD = "static/";

module.exports = function(grunt) {
    grunt.initConfig({
        PATH_SRC_JS     : PATH_SRC + 'js/',   // in the variable, include trailing slash "/"
        PATH_SRC_SASS   : PATH_SRC + 'sass/',
        PATH_SRC_IMG    : PATH_SRC + 'img/',

        PATH_BUILD_JS   : PATH_BUILD + "js/",
        PATH_BUILD_SASS : PATH_BUILD + "css/",
        PATH_BUILD_IMG  : PATH_BUILD + "img/",

        PATH_BUILD_SASS_FILENAME    : "site", // exclude .css
        PATH_BUILD_JS_FILENAME      : "global", // exclude .js

        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            all: [ '<%= PATH_SRC_JS %>partials/*.js' ],
            options: { devel: true },
            global: { $: true }
        },

        clean: {
            cleanStyles: [ '<%= PATH_BUILD_SASS %><%= PATH_BUILD_SASS_FILENAME %>.css', '<%= PATH_BUILD_SASS_FILENAME %>.css.map' ],
            cleanScript: [ '<%= PATH_SRC_JS %>build/*.js' ]
        },

        concat: {
            the_scripts: {
                 files: {
                    '<%= PATH_BUILD_JS %><%= PATH_BUILD_JS_FILENAME %>.js': [ '<%= PATH_SRC_JS %>libs/*.js', '<%= PATH_SRC_JS %>partials/*.js' ],
                },
            }
        },

        uglify: {
            options: {
                mangle: false
            },
            build: {
                src: '<%= PATH_BUILD_JS %><%= PATH_BUILD_JS_FILENAME %>.js',
                dest: '<%= PATH_BUILD_JS %><%= PATH_BUILD_JS_FILENAME %>.min.js'
            }
        },

        cssmin: {
            minify: {
                expand: true,
                cwd: '<%= PATH_BUILD_SASS %>',
                src: ['*.css', '!*.min.css'],
                dest: '<%= PATH_BUILD_SASS %>',
                ext: '.min.css'
            }
        },

       sass: {
            options: { sourceMap: true },
            compile: {
                files: {
                    '<%= PATH_BUILD_SASS %>site.css': '<%= PATH_SRC_SASS %>*.scss',
                    '<%= PATH_BUILD_SASS %>summer2015.css': '<%= PATH_SRC_SASS %>summer.scss'
                }
            }
        },

        autoprefixer: {
            options: {
                diff: true,
                map: true,
            },
            multiple_files: {
                src: '<%= PATH_BUILD_SASS %>*.css',
            },
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: '<%= PATH_SRC_IMG %>',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: '<%= PATH_BUILD_IMG %>',
                }]
            },
        },
        svgstore: {
            options: {
                prefix : 'shape-', // This will prefix each ID
                svg: { // will add and overide the the default xmlns="http://www.w3.org/2000/svg" attribute to the resulting SVG
                    viewBox : '0 0 100 100',
                    xmlns: 'http://www.w3.org/2000/svg'
                }
            },
            your_target: {
                // Target-specific file lists and/or options go here.

            },
        },

        watch: {
            options: {
                livereload: true,
            },

             styles: {
                files: ['<%= PATH_SRC_SASS %>**/*.scss'],
                tasks: ['sass', 'autoprefixer','cssmin'],
            },

            images: {
                files: ['<%= PATH_SRC_IMG %>'],
                tasks: ['imagemin'],
            },


            scripts: {
                files: [ '<%= PATH_SRC_JS %>**/*.js'],
                tasks: ['jshint', 'concat', 'uglify'],
                options: {
                    spawn: false,
                },
            },
        }
    });


    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-svgstore');

    grunt.registerTask('default', [
       'clean',
        'sass','autoprefixer','cssmin',
        'watch'
        ]);

    grunt.registerTask('live', [
        'clean',
        'jshint',
        'concat',
        'uglify',
        'cssmin']);

    grunt.registerTask('imgmin', [
        'imagemin']);

};
