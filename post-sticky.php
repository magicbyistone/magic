
<article class="post-item-sticky">
	<div class="post-sticky-content">
		<h2>
			<a href="<?php the_permalink(); ?>">
				<?php the_title(); ?>
			</a>
		</h2>

		<?php get_template_part('post', 'author'); ?>
	</div>
	<?php the_post_thumbnail('large'); ?>
</article>