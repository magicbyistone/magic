<?php get_template_part('header-init'); ?>

<header class="header">

	<a class="header-logo" href="<?php echo site_url(); ?>">
		<img class="header-logo-img-black" src="<?php bloginfo('template_url'); ?>/static/img/logo/magiclogo-black.png">
		<img class="header-logo-img-white" src="<?php bloginfo('template_url'); ?>/static/img/logo/magiclogo-white.png">
		</a>
	</div>

	<div class="container">


	<button type="button" class="fullscreen-nav-toggler navicon-button x" data-action="toggle-nav">
		<i class="navicon"></i>
	</button>

	<div class="fullscreen-nav">
		<div class="fullscreen-nav-container">

		<?php wp_nav_menu( array(
			'theme_location'  => mainnav,
			'container'       => 'nav',
			'container_class' => 'navigation',
			'menu_class'      => 'mainnav menu',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 0 ) ); ?>


		<section class="contact-actions">
			<div class="container">
				<section class="contact-social">

					<a href="mailto:hello@magicbyistone.se?subject=I%20love%20magic">
						<svg version="1.1" class="svg-mail" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 30.5 30.5" enable-background="new 0 0 30.5 30.5" xml:space="preserve">
						<g>
							<path  d="M29.9,0.6C29.5,0.2,28.9,0,28.4,0c-0.5,0-1,0.2-1.4,0.6l-4.3,4.3V2.8H0v27.7h27.7V7.8h-2.2l4.3-4.3
								C30.7,2.7,30.7,1.4,29.9,0.6z M25.4,28.2H2.3V5.1h20.2L10.1,17.5v2.9h2.8L25.4,7.9V28.2z M28.9,2.5L12.3,19.1h-0.9v-1L27.9,1.6
								c0.2-0.2,0.4-0.2,0.5-0.2c0.1,0,0.3,0,0.5,0.2c0.2,0.2,0.2,0.4,0.2,0.5C29.1,2.1,29.1,2.4,28.9,2.5z"/>
							<path  d="M12.1,24.1c-0.1,0-0.2,0-0.3,0c-1.5-0.1-2.6-0.5-3.1-1.3c-0.5-0.7-0.4-1.5-0.3-1.8l1,0.3l-0.5-0.2l0.5,0.2
								c0,0-0.1,0.5,0.2,0.9c0.3,0.5,1.1,0.8,2.3,0.8c1.6,0.1,2-0.6,2.5-1.6c0.5-0.9,1.1-2,2.9-2.4c1.1-0.2,2-0.1,2.8,0.5
								c1.2,1,1.3,2.7,1.3,2.8l-1.1,0.1c0,0-0.1-1.3-0.9-2c-0.5-0.4-1.1-0.4-1.9-0.3c-1.3,0.3-1.7,1-2.2,1.9C14.8,23,14.2,24.1,12.1,24.1z
								"/>
						</g>
						</svg>
					</a>

					<a href="https://www.facebook.com/magicbyistone	">
						<svg version="1.1" class="svg-facebook" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 12 26.7" enable-background="new 0 0 12 26.7" xml:space="preserve">
						<path  d="M6,26.7H3V8.5C3,0.6,6.6,0,11.6,0v2.8C7.2,2.8,6,3.6,6,8.5V26.7z"/>
						<rect y="8.7"  width="12" height="2"/>
						</svg>
					</a>
					<a href="https://www.linkedin.com/company/magic-by-istone">
						<svg version="1.1" class="svg-linkedin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 26 25.1" enable-background="new 0 0 26 25.1" xml:space="preserve">
						<rect y="5.1"  width="3" height="20"/>
						<rect x="8" y="5.1"  width="3" height="20"/>
						<path  d="M26,25.1h-3V13.7c0-3.9-2-5.9-6-5.9c-3.3,0-6,2.7-6,5.9v11.4H8V13.7c0-4.9,4-8.9,8.9-8.9
							c5.7,0,9.1,3.3,9.1,8.9V25.1z"/>
						<circle  cx="1.5" cy="1.5" r="1.5"/>
						</svg>
					</a>
					<a href="https://twitter.com/magicbyistone">
						<svg version="1.1" class="svg-twitter" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 33.1 29.9" enable-background="new 0 0 33.1 29.9" xml:space="preserve">
						<g>
							<path  d="M13.7,29.9c-3.5,0-6.9-1.1-9.7-3.2l-2.4-1.8l3,0c0,0,2.4,0,4.8-0.4c-1.9-0.6-4.1-1.8-5.6-4.2L3,19.1
								l2.8-0.6c-2-1.4-4.6-4-5.4-9.1L0,7.3l1.8,1c0.1,0,5.2,2.8,9.5,1.9V7.7C11.3,3.5,14.8,0,19,0c1.9,0,3.7,0.7,5.1,1.9l-1.3,1.5
								C21.8,2.5,20.4,2,19,2c-3.1,0-5.7,2.6-5.7,5.7v4L12.6,12C9,13.1,5,11.9,2.7,11c1.7,5.6,6.2,6.8,6.2,6.8l4.1,1l-6.7,1.5
								c2.8,3,7.2,2.7,7.2,2.7l4-0.2l-3.4,2.1c-1.7,1-4.3,1.5-6.3,1.8c1.8,0.8,3.8,1.2,5.8,1.2c7.8,0,14.1-6.3,14.1-14
								c0-1.9-0.4-3.8-1.2-5.6l1.8-0.8c0.9,2,1.4,4.2,1.4,6.4C29.7,22.8,22.5,29.9,13.7,29.9z"/>
							<path  d="M24.5,5.1C24,4.5,23.5,4,22.9,3.4l1.3-1.5c0.7,0.6,1.3,1.3,1.9,1.9L24.5,5.1z"/>
							<polygon  points="26.9,9.2 26.4,8.3 30,6.6 26.8,5.9 27.7,3 24.6,5.2 24,4.4 29.6,0.5 28.1,5.2 33.1,6.3 	"/>
						</g>
						</svg>
					</a>
					<a href="https://instagram.com/magicbyistone/">
						<svg version="1.1" class="svg-instagram" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 29.1 30" enable-background="new 0 0 29.1 30" xml:space="preserve">
						<path  d="M22,30H6.4C2.6,30,0,26.7,0,21.9v-14C0,3.2,2.6,0,6.4,0H25v2H6.4C3.7,2,2,4.3,2,7.8v14C2,24.9,3.4,28,6.4,28
							H22c2.8,0,5-2.7,5-6.1V4h2v17.9C29,26.4,25.9,30,22,30z"/>
						<path  d="M14.2,23.8c-4.9,0-8.9-4-8.9-8.9s4-8.9,8.9-8.9c4.9,0,8.9,4,8.9,8.9S19.1,23.8,14.2,23.8z M14.2,7.9
							c-3.8,0-6.9,3.1-6.9,6.9c0,3.8,3.1,6.9,6.9,6.9c3.8,0,6.9-3.1,6.9-6.9C21.1,11,18,7.9,14.2,7.9z"/>
						<path  d="M24.6,9c-1.2,0-2.3-0.5-3.2-1.3s-1.3-2-1.3-3.2c0-1.2,0.5-2.3,1.3-3.2s2-1.3,3.2-1.3s2.3,0.5,3.2,1.3
							s1.3,2,1.3,3.2c0,1.2-0.5,2.3-1.3,3.2S25.8,9,24.6,9z M24.6,2c-0.7,0-1.3,0.3-1.8,0.7c-0.5,0.5-0.7,1.1-0.7,1.8
							c0,0.7,0.3,1.3,0.7,1.8c0.9,0.9,2.6,0.9,3.5,0c0.5-0.5,0.7-1.1,0.7-1.8c0-0.7-0.3-1.3-0.7-1.8C25.9,2.3,25.2,2,24.6,2z"/>
						<rect x="1" y="15"  width="5" height="1"/>
						<rect x="22" y="15"  width="6" height="1"/>
						</svg>
					</a>
				</section>
			</div>
		</section>


		<section class="nav-etc">
			<a href="/monster-love-cookies-right">Do you even cookies?</a>
			<a href="http://istone.com">Visit iStone Group now.</a>
		</section>
		</div>
	</div>


		<script type="text/javascript">
			var hamb = document.querySelector('.fullscreen-nav-toggler');
			var body = document.body;

			hamb.addEventListener('click', toggleNav);

			function toggleNav() {
				hamb.classList.toggle('open');
				body.classList.toggle('js-nav-visible');
			}
		</script>

</header>
