<?php /* Template name: Cases */ ?>
<?php get_header(); ?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.8.6/jquery.fullPage.min.css" media="screen" title="no title">

<div id="menu"></div>

<style media="screen">
	body {
		background: black;
	}

	.section {
		position: relative;
		background-size: auto 100%;
		background-repeat: no-repeat;
		background-position: center center;
		background-size: cover;
		transition: margin 10ms ease-in;
	}

	@media (min-width: 768px) {
		.section {
			background-size: 100% auto;
			background-size: 100%;
			background-size: cover;
		}
	}

	.content {
		color: white;
		position: absolute;
		top: 80px;
		right: calc(100% - 80px);
		transition: all 300ms 120ms ease-in;
		max-width: 70%;
		width: 480px;
		overflow: hidden;
	}

	.content.content-is-visible {
		right: calc(100% - 80%);
		transition: all 200ms 120ms ease-out;
		width: 480px;
	}

	@media (min-width: 768px) {
		.content {
			top: 40%;
		}

		.content.content-is-visible {
			right: calc(100% - 520px);
		}
	}

	.content-wrapper {
		background: #00b1af;
		pointer-events: none;
		padding: 15px 51px 25px 15px;
		opacity: 0;
		transition: all 400ms 180ms ease-in;
	}

	.content-is-visible .content-wrapper {
		pointer-events: auto;
		opacity: 1;
		transition: all 200ms 180ms ease-out;
	}

	.content-title:after {
		content: " ";
		background: white;
		width: 25%;
		height: 2px;
		margin: 15px 0;
		display: block;
	}

	.content-toggler {
		position: absolute;
		top: 5px;
		right: 5px;
		width: 44px;
		height: 44px;
		cursor: pointer;
		z-index: 30;
		animation: rotateLeft 200ms ease-in;
		animation-fill-mode: backwards;
		background: #00b1af;
	}

	.content-toggler:after {
		position: absolute;
		content: " ";
		background: url('<?php bloginfo("template_url"); ?>/src/img/magic_cross.svg') center center no-repeat;
		background-size: 80% 80%;
		transform: rotate(45deg);
		top: 15%;
		left: 15%;
		width: 70%;
		height: 70%;
	}

	.content-is-visible .content-toggler {
		animation: rotateRight 150ms ease-out;
		animation-fill-mode: forwards;
		background: transparent;
		background: #00b1af;
	}
	@keyframes rotateRight {
		to { transform: rotateZ(45deg); }
	}
	@keyframes rotateLeft {
		from { transform: rotateZ(45deg); }
		to { transform: rotateZ(0deg); }
	}
</style>


<main id="fullpage">
	 <?php if( have_rows('cases') ): ?>
		 <?php $index = 0; ?>
	     <?php while( have_rows('cases') ): the_row();
			   $img = get_sub_field('image'); ?>

			 <section class="section" data-anchor="<?php the_sub_field('name'); ?>"
				 style="background-image: url('<?php echo $img['url'] ?>');">
				 <article class="content">

					 <a target="_self" class="content-toggler">
					 	<i class="cross"></i>
					 </a>

					 <div class="content-wrapper">
				         <h2 class="content-title"><?php the_sub_field('title'); ?>,</h2>
						 <?php the_sub_field('body'); ?>
					 </div>
				 </article>


				 <style media="screen">

				 	.active .section-next {
				 		opacity: 0.1;
						transition: all 500ms ease-in;
				 	}
				 	.section-next {
						color: #999;
				 		opacity: 1;
						transition: all 250ms ease-in;
				 		height: 60px;
						position: absolute;
						top: -60px;
						bottom: 100%;
						width: 100%;
						left: 0;
						background-repeat: no-repeat;
						background-position: center top;
						background-size: cover;
				 	}

					.section-next:hover {
						color: white;
					}

					.section-next:after {
						content: " ";
						background-color: rgba(0,0,0,0.8);
						z-index: 10;
						position: absolute;
						top: 0; left: 0;
						width: 100%; height: 100%;
					}

				 	.button-next-section {
				 		width: 52px;
						height: 52px;
						border-radius: 50%;
						text-align: center;
						background: #00b1af;
						display: block;
						margin: 0 auto;
						position: relative;
						top: -50%;
						z-index: 40;
				 	}

					.button-next-section:hover {
						animation: bounce 340ms infinite ease-in alternate;
					}

					@keyframes bounce {
						0% {
							transform: translateY(-20%);
						}
						100% {
							transform: translateY(40%);
						}
					}

					.button-next-section img {
						display: block;
						max-width: 20px;
						max+height: 20px;
						position: absolute;
						left: 50%;
						top: 50%;
						transform: translate(-50%, -50%);
					}

					.section-next-title {
						position: absolute;
						top: 0;
						left: 20px;
						line-height: 60px;
						z-index: 15;
						color: inherit;
					}

				 </style>

	 			<a href="#<?php the_sub_field('name'); ?>" class="section-next" style="background-image: url('<?php echo $img['url'] ?>');">
					<h2 class="section-next-title"><?php the_sub_field('name'); ?></h2>

	 				<div class="button-next-section">
						<img src="<?php bloginfo('template_url'); ?>/src/img/arrow.down.svg">
	 		   		</div>
				</a>

		 	</section>

	     <?php endwhile; ?>
	 <?php endif; ?>
</main>

<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="   crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.8.6/jquery.fullPage.min.js" charset="utf-8"></script>
<script type="text/javascript">
	$('#fullpage').fullpage();

	var ct = document.querySelectorAll('.content-toggler');
	for (var i = 0; i< ct.length; i++) {
		ct[i].addEventListener('click', toggleContent);
	}

	function toggleContent(event) {
		event.target.parentElement.classList.toggle('content-is-visible');
	}
</script>


<?php get_footer(); ?>
