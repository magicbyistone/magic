<?php /* Template name: Startpage-dynamic */ ?>
<?php get_header('start'); ?>

<style type="text/css">

.splashBG {
    background: url('<?php the_field('image-phone'); ?>') no-repeat 50% 50% fixed;
}

    @media (max-width: 600px) {
        .splashBG {
            background: url('<?php the_field('image-phone'); ?>') no-repeat 50% 50% fixed;
        }
    }
    @media (max-width: 600px) and (orientation : portrait) {
        .splashBG {
            background-size: auto 100%;
        }
    }
    @media (max-width: 660px) and (orientation: landscape ) {
        .splashBG {
             background-size: 100% auto;
        }
    }

    @media (min-width: 601px) and (max-width: 992px) {
        .splashBG {
            background: url('<?php the_field('image-tablet'); ?>') no-repeat center center;
            background-size: cover;
        }
    }

    @media (min-width: 993px) {
        .splashBG {
            background: url('<?php the_field('image-desktop'); ?>') no-repeat center center;
            background-size: cover;
        }
    }

</style>



 <article class="new-startpage splashBG">

     <h1 class="startpage-title">
         <a href="<?php the_field('small-heading-link'); ?>">
             <?php the_field('small-heading-text'); ?>
         </a>
     </h1>

     <h2 class="h1">
         <?php the_field('big-heading'); ?>
     </h2>

</article>


<?php get_footer(); ?>


<script>
function setSize(el, w, h) {
    el.style.height = h+'px';
    el.style.width = w+'px';
}

window.onload = function() {
    var splash = document.querySelectorAll('.splashBG')[0];

    var vh = window.innerHeight;
    var vw = window.innerWidth;

    setSize(splash, vw, vh);

    window.addEventListener('resize', function() {
        vh = window.innerHeight;
        vw = window.innerWidth;

        setSize(splash, vw, vh);
    });
};
</script>
