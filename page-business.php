<?php /* Template Name: Business */ ?>
<?php get_header(); ?>

	<article class="container">

		<h1 class="page-title"><?php the_field('title'); ?></h1>

		<?php
		/*
				ACF Repeater for:
				Business information
		*/

		if( have_rows('bus') ): ?>
		<ol class="business-list">
		<?php 	// loop through the rows of data
		    while ( have_rows('bus') ) : the_row(); ?>
				<li>
					<h2><?php the_sub_field('title'); ?></h2>
					<?php the_sub_field('text'); ?>
				</li>
		    <?php endwhile; ?>
		 </ol>
		<?php endif; ?>


		<div class="business-logos">
			<img src="<?php bloginfo('template_url'); ?>/static/img/logoblock.png" alt="" />

			<div class="business-cta-container">
				<a href="mailto:anja@magicbyistone.se" class="button-ghost-cta centered">We miss your logo!</a>
			</div>
		</div>

		<section class="business-messages">
			<div class="business-message-item">

				<h3 class="h2"><?php the_field('bus-title') ?></h3>
				<p><?php the_field('bus-text-1'); ?></p>
			</div>

			<div class="business-message-item">
				<h3 class="h2"><?php the_field('bus-title2') ?></h3>
				<p><?php the_field('bus-text-2'); ?></p>
			</div>
		</section>

		<div class="business-cta-container">
			<a data-toggle="contact" href="<?php the_field('business_cta_link'); ?>" class="button-ghost-cta centered"><?php the_field('bus-cta-text'); ?></a>
		</div>

	</article>


<?php if( get_field('promotion-title')): ?>
	<article class="business-istone">
		<div class="container">
			<h2 class="h1 promotion-title"><?php the_field('promotion-title'); ?></h2>

			<div class="business-message-item">
				<?php the_field('promotion-text') ?>
			</div>

			<div class="business-message-item">
				<?php the_field('promotion-text2') ?>
			</div>
		</div>

	</article>
<?php endif; ?>


	<article class="business-pink">
		<a class="button-ghost" href="http://www.istone.com/se">Go visit our friends at iStone HQ</a>
	</article>


<?php get_footer(); ?>
