<!DOCTYPE html>
<!--[if IE 8]><html  class="  lt-ie9 <?php if(is_front_page()) { echo "is-frontpage"; } else { echo "otherpage"; } ?>"  lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html class="<?php if(is_front_page()) { echo "is-frontpage"; } else { echo "otherpage"; } ?>"  lang="en"> <!--<![endif]-->
<head>
	<title>Magic by iStone</title>
	<meta charset="UTF-8" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/static/css/site.css?v=20161004">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="Shortcut Icon" type="image/x-icon" href="<?php bloginfo('template_url') ?>/static/img/favicon.ico">
	<!--[if lt IE 9]>
      <script src="<?php bloginfo('template_url'); ?>/static/js/libs/html5shiv.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
		var templateUrl = '<?= get_bloginfo("template_url"); ?>';
		var siteUrl = '<?= site_url(); ?>';
	</script>
   	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NNMGQK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NNMGQK');</script>
<!-- End Google Tag Manager -->
