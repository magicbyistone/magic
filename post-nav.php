<?php 
/* 
	
	Post navigation

*/
$prev_post = get_previous_post();
$next_post = get_next_post();

if ( $prev_post || $next_post ) { 
?><nav class="post-nav">
<?php if (!empty( $prev_post )): ?>
		<a class="post-nav-prev " href="<?php echo get_permalink( $prev_post->ID ); ?>">
			<span>Previous post</span>
	  		<h2 class="post-nav-title">
		  		<?php echo $prev_post->post_title; ?>
			</h2>
		</a>
	<?php endif; ?>

	<?php
	
	if (!empty( $next_post )): ?>
	  	<a class="post-nav-next" href="<?php echo get_permalink( $next_post->ID ); ?>">
		  	<span>Next post</span>
			<h2 class="post-nav-title ">  	
		  		<?php echo $next_post->post_title; ?>
		  	</h2>
		  
  		</a>
	<?php endif; ?>
</nav>

<?php }  // end of post nav ?>