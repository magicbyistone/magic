<?php 
//is sticky 
$sticky = get_option( 'sticky_posts' );
$args = array(
    'posts_per_page' => 2, //get all post
    'post__in'  => $sticky, //are they sticky post
);

// The Query
$sticky_posts = new WP_Query( $args );

// The Loop //we are only getting a list of the title as a li see the loop docs for details on the loop or copy this from index.php (or posts.php)
if ( $sticky_posts->have_posts() ): ?>
<section class="post-list-sticky">
	<div class="sticky-scroll-container">
		<div class="sticky-scroll-content">
			<?php while ( $sticky_posts->have_posts() ):
			    $sticky_posts->the_post();
			    get_template_part('post', 'sticky');
			endwhile; ?>
		</div>
	</div>
</section>
<?php endif;
wp_reset_query(); //reset the original WP_Query ?>
