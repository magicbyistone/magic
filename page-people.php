<?php /* Template name: People  */ ?>
<?php get_header(); ?>


<style media="screen">
#images-list {
  overflow: hidden; }

.images__item {
  position: relative;
  display: inline-block;
  width: 25%;
  vertical-align: top;
  padding-bottom: 25%;
  opacity: 1;
  -webkit-transition: opacity 300ms ease;
          transition: opacity 300ms ease; }
  .images__item:hover {
    cursor: pointer;
    opacity: .8; }
  .images__item .images__img-holder {
    position: absolute;
    right: 0;
    top: 0;
    left: 0;
    bottom: 0;
    -webkit-transform-style: preserve-3d;
            transform-style: preserve-3d; }
    .images__item .images__img-holder.rotate {
      -webkit-transform: rotateY(-90deg);
              transform: rotateY(-90deg);
      -webkit-transition: -webkit-transform 500ms ease;
              transition: transform 500ms ease; }
    .images__item .images__img-holder .images__first-img,
    .images__item .images__img-holder .images__second-img {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      opacity: 1;
      background-position: center center;
      background-repeat: no-repeat;
      background-size: cover;
      -webkit-backface-visibility: hidden; }

</style>

<section class="people">
	<h1><?php the_field('people_title'); ?></h1>

<?php
	// check if the repeater field has rows of data
if( have_rows('people_list') ): ?>
	<div class="people-list clearfix">
 	<?php // loop through the rows of data
    while ( have_rows('people_list') ) : the_row(); ?>
 	<article class="persona">
 		<?php $image = get_sub_field('image'); ?>
    	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
    	<div class="info">
 			<h2 class='persona-name'><?php the_sub_field('name') ?></h2>
 			<div class='persona-info'><?php the_sub_field('info') ?></div>
 		</div>
    </article>
    <?php endwhile; ?>
	</div>
<?php endif; ?>
</section>

<div class="people-contact-block clearfix">
	<h2 class="h1"><?php the_field('people-hire-title'); ?></h2>
	<a href="<?php the_field('people-hire-link') ?>"
		class="button-ghost-cta"><?php the_field('people-hire-text') ?></a>
</div>


<?php /* Gets populated from footer scripts */  ?>
<?php $images = get_field('random_images');
if( $images ): ?>
	<ul id="images-list" class="clearfix"></ul>
	<script type="text/javascript">
        var imagesArr = [ <?php foreach( $images as $image ): ?><?php echo '"' . $image['sizes']['large'] . '", ' ;  ?><?php endforeach; ?>
    	];
	</script>
<?php endif; ?>


<?php get_footer(); ?>

<script type="text/javascript">

// The amount of pictures we want to be printed out
var nrImages = 24;

var zindex = 1;

// One array for the available ones and the added ones,  so we
// never have the same ones.
var availableImages = [];
var addedImages = [];


function createImgElement(size) {
	var index = 0;
	var item = document.createElement('li');
	item.className = 'images__item';
	item.style.width = size;

	// Setting padding-bottom to the same value as the width will force the ratio of 1:1
	// no matter what the window width/height is.
	item.style.paddingBottom = size;

	// Create image placdeholders
	var ImgPlaceholder = document.createElement('div');
	ImgPlaceholder.className = 'images__img-holder';
	var imgFirst = document.createElement('div');
	imgFirst.className = 'images__first-img';
	var imgSecond = document.createElement('div');
	imgSecond.className = 'images__second-img';

	// Pick random image from the images array that's not as the last one.
	index = Math.floor(Math.random() * (nrImages - 1) );

	item.addEventListener('click', function(e) {
		changeImg(e.target.parentElement.parentElement);
	});

	imgFirst.style.backgroundImage = "url('" + availableImages[index] + "')";
	addedImages.push(availableImages[index]);
	availableImages.splice(availableImages.indexOf(availableImages[index]), 1);

	ImgPlaceholder.appendChild(imgFirst);
	ImgPlaceholder.appendChild(imgSecond);
	item.appendChild(ImgPlaceholder);

	document.getElementById('images-list').appendChild(item);

	imgFirst.style.transform = 'translateZ('+ ImgPlaceholder.offsetWidth/2 + 'px)';
}

function changeImg(image) {
	if (!image || !image.children[0].children[0] || !image.children[0].children[1] || image.changing) {
		return;
	}

	// Semi hack to make the current image the highest z-index. Otherwise it will not we shown as a cube is a sibling is also active
	zindex++;
	image.style.zIndex = zindex;

	image.changing = true;
	var newImg = availableImages[ Math.floor(Math.random() * (availableImages.length - 1) ) ];
	var oldImg = getComputedStyle(image.children[0].children[0]).getPropertyValue("background-image").split('"')[1];

	image.children[0].children[0].style.transform = 'translateZ('+ image.offsetWidth/2 + 'px)';
	image.children[0].children[1].style.transform = 'rotateY(-270deg) translateZ(' + image.offsetWidth/2 + 'px)';

	image.children[0].children[1].style.backgroundImage = "url('" + newImg + "')";
	image.children[0].classList.add('rotate');

	// Reset everything when animation is done.
	setTimeout(function() {
		image.children[0].children[0].style.backgroundImage = "url('" + newImg + "')";
		image.children[0].classList.remove('rotate');

		addedImages.push( newImg );
		availableImages.splice( availableImages.indexOf(newImg), 1);

		availableImages.push( oldImg );

		image.changing = false;
	}, 1000);
}


// Initial print out if images
////////////////////////////////////////////
while (nrImages > 0) {
	if (typeof imagesArr == 'undefined') {
		break;
	}

	availableImages = imagesArr || [];

	if (window.innerWidth > 1300) {
		size = '16.666666667%';
	} else if (window.innerWidth > 600) {
		size = '25%';
	} else {
		size = '50%';
	}

	createImgElement(size);
	nrImages--;
}

// Interval for infinite loop of changing images
////////////////////////////////////////////

setInterval(function() {
	var images = document.getElementsByClassName('images__item');
	var image = images[Math.floor(Math.random() * images.length)];

	changeImg(image);
}, 3000);

</script>
