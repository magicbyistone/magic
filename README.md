# Magic wp theme
Hosted on bitbcket under the magic team.

## Dependancies
- Grunt
- Node Sass
- JSHinting
- Js concatenation


## Plugins
- ACF Pro
- Contact form 7 
-- Contact form 7 to DB
-- Contact form 7 Honeypot
- All-in-one SEO

## Status
[![Deployment status from dploy.io](https://magicbyistone.dploy.io/badge/23779029959970/22039.png)](http://dploy.io)

http://magicbyistone.dploy.io