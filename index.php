<?php get_header(); ?>

	<section class="post-list-intro">
		<h1 class="page-title"><?php the_field('blog-title', get_option('page_for_posts')); ?></h1>

		<?php if(get_field('blog-intro', get_option('page_for_posts'))): ?>
			<p><?php the_field('blog-intro', get_option('page_for_posts')); ?></p>
		<?php endif; ?>
	</section>

	<section class="post-list">
		<?php if(have_posts() ): while(have_posts()): the_post(); ?>

			<article class="post-item">
			<?php
			$thumb_id = get_post_thumbnail_id();
			$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
			?>

				<?php if(has_post_thumbnail()): ?>
					<img src="<?php echo $thumb_url[0]; ?>"/>
					<div class="post-content-relative">
						<h1 class="post-title"><?php the_title(); ?></h1>
						<?php the_content(); ?>
					</div>
				<?php endif; ?>

				<?php if (!has_post_thumbnail()): ?>
					<div class="post-content-relative">
						<header>
							<h1 class="post-title"><?php the_title(); ?></h1>
						</header>
						<?php the_content(); ?>
					</div>
				<?php endif; ?>
			</article>

	    <!-- Add the pagination functions here. -->



		<?php endwhile; ?>
		
		<nav class="postnav">
			
			<div class="postnav__older">
				<?php next_posts_link( 'Older posts' ); ?>
			</div>
			
			<div class="postnav__newer">
				<?php previous_posts_link( 'Newer posts' ); ?>
			</div>

		</nav>
		<?php endif; ?>

	</section>

<?php get_footer(); ?>
