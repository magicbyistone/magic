<?php /*  Tmeplate for displaying single-post */ ?>
<?php get_header(); ?>

<?php if(have_posts() ): while(have_posts()): the_post(); ?>

	<div id="post">
	<article class="post-single">

	<?php
	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
	?>

		<?php if(has_post_thumbnail()): ?>

			<header class="post-header fullscreen-section" style="background-image: url('<?php echo $thumb_url[0]; ?>');">
				<div class="fullscreen-section-content">

					<div class="container">
						<h1 class="post-title"><?php the_title(); ?></h1>
						<?php get_template_part('post', 'author'); ?>
					</div>
				</div>
			</header>
			<div class="post-single-content post-content-relative">
				<h2 class="post-title h1"><?php the_title(); ?></h2>
				<?php the_content(); ?>
			</div>
		<?php endif; ?>

		<?php if (!has_post_thumbnail()): ?>
			<div class="post-single-content post-content-relative">
				<header>
					<h1 class="post-title"><?php the_title(); ?></h1>
				</header>
				<?php the_content(); ?>
			</div>
		<?php endif; ?>
	</article>

	<?php get_template_part('post', 'nav');  ?>

	</div>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
